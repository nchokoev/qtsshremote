#ifndef __SSHCONTROL_H__
#define __SSHCONTROL_H__
#include "libssh2.h"

int connectHost(char *hostname, char *username, char *password);
void disconnectHost(void);

void sendFullScreen(void);
void sendPause(void);
void sendStop(void);
void sendPlay(void);
void sendNormalScreen(void);
void sendOpen(char *path);
void sendVolumeUp(void);
void sendVolumeDown(void);

/* returns number of PIDs
 * takes *ps - process name
 * returns *pids - list of PIDs (coma, '\n', or other separates)
 */
int getPIDsFor(char *ps, char *pids);
/* cmd - command
 * len - command length in chars
 * buf - pointer to a buffer where the output will be stored
 * buflen - size of the buffer in chars
 * returns the number of chars writen
 */
int execSSHCmd(char *cmd, int len, char *buf, int buflen);
const char *getPromptString();

#endif
