#ifndef QTSSHREMOTE_H
#define QTSSHREMOTE_H

#include <QtWidgets/QMainWindow>
#include "ui_qtsshremote.h"
#include"sshcontrol.h"

class QtSSHRemote : public QMainWindow
{
	Q_OBJECT

public:
	QtSSHRemote(QWidget *parent = 0);
	~QtSSHRemote();

private:
	Ui::QtSSHRemoteClass ui;
	bool flagConnected;
	void uiEnableConnected();
	void uiDisableConnected();
	void refreshFolders();
	QStringList getMPlayerRunning();
	void checkMplayerRunning();

private slots:
	void connectClicked();
	void openClicked();
	void playClicked();
	void stopClicked();
	void pauseClicked();
	void fullScreenClicked();
	void disconnectClicked();
	void itemDoubleClicked(QListWidgetItem * item);
	void startmplayerClicked();
	void killmplayerClicked();
	void normalScreenClicked();
	void volumeUpScreenClicked();
	void volumeDownScreenClicked();

};

#endif // QTSSHREMOTE_H
