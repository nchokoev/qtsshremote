#include"sshcontrol.h"

static const char *promptString = "<cp>$";
static const char *nlPromptString1 = "\n<cp>$";
static const char *nlPromptString2 = "\r<cp>$";
static unsigned long hostaddr;
static int sock;
static LIBSSH2_SESSION *session;
static LIBSSH2_CHANNEL *channel;
static int flagConnected = 0;
static 	char buffer[0x4000];

const char *getPromptString()
{
	return promptString;
}

int endsWith(const char *str, const char *suffix)
{
	size_t lenstr;
	size_t lensuffix;
	if (!str || !suffix)
		return 0;
	lenstr = strlen(str);
	lensuffix = strlen(suffix);
	if (lensuffix >  lenstr)
		return 0;
	return strncmp(str + lenstr - lensuffix, suffix, lensuffix) == 0;
}

int waitsocket(int socket_fd, LIBSSH2_SESSION *session)
{
	int rc;
	struct timeval timeout;
	fd_set fd;
	fd_set *writefd = NULL;
	fd_set *readfd = NULL;
	int dir;
 
	timeout.tv_sec = 10;
	timeout.tv_usec = 0;
 
	FD_ZERO(&fd);
 
	FD_SET(socket_fd, &fd);
 
	/* now make sure we wait in the correct direction */ 
	dir = libssh2_session_block_directions(session);

 
	if(dir & LIBSSH2_SESSION_BLOCK_INBOUND)
	{
		readfd = &fd;
	}
 
	if(dir & LIBSSH2_SESSION_BLOCK_OUTBOUND)
	{
		writefd = &fd;
	}
 
	rc = select(socket_fd + 1, readfd, writefd, NULL, &timeout);
 
	return rc;
}

void prepend(char* s, const char* t)
{
	size_t len = strlen(t);
	size_t i;

	memmove(s + len, s, len + 1);

	for (i = 0; i < len; ++i)
	{
		s[i] = t[i];
	}
}

void append(char* s, const char* t)
{
	size_t lent = strlen(t);
	size_t lens = strlen(s);
	size_t i;


	for (i = 0; i < lent; ++i)
	{
		s[i+lens] = t[i];
	}
}

static void sshSetPrompt()
{
	char* s;

	int pslen = strlen(promptString);
	int cmdlen = strlen("PS1=\"");
	int endslen = strlen("\"\n");
	int slen = pslen + cmdlen + endslen + 1;
	s = (char*)malloc(slen);
	if(s)
	{
		memset(s, 0, slen);

		strncpy(s, promptString, pslen);
		prepend(s, "PS1=\"");
		append(s, "\"\n\0");
		execSSHCmd(s, slen, s, 0);
		delete (s);
	}
}

int stringContains(char *s, const char *t)
{
	int slen = strlen(s);
	int tlen = strlen(t);
	int is;
	int it;

	for(is=0; is <= (slen-tlen); is++)
	{
		if(!strncmp(&s[is], t, tlen))
		{
			return 1;
		}
	}
	return 0;
}

void waitForString(char *str)
{
	int rc;
	int tmout = 0;
	int bytecount = 0;
	int strl = strlen(str);
	int res = 0;

	do
	{
		rc = libssh2_channel_read(channel, &buffer[bytecount], sizeof(buffer)-bytecount);

		if( rc > 0 )
		{
			bytecount += rc;
			//checkfor string
			if(bytecount > strl)
			{
				buffer[bytecount] = '\0';
				res = stringContains(buffer, str);
				if(res != 0)
				{
					break;
				}
			}

		}
		else if (rc == LIBSSH2_ERROR_EAGAIN)
		{
			waitsocket(sock, session);
			tmout++;
		}
		if(tmout > 5)
		{
			break;
		}
	}
	while(1);
}

int execSSHCmd(char *cmd, int len, char *buf, int buflen)
{
	int rc;
	int bytecount = 0;
	int iTimeout = 0;
	int i;

	if(flagConnected == 0)
	{
		return 0;
	}

	libssh2_channel_flush(channel);
//	QThread::msleep(100);
	//read out everything left
	do
	{
		rc = libssh2_channel_read(channel, buffer, sizeof(buffer));

		if( rc > 0 )
		{
//			qDebug() << "We read: " << rc;
		}
		else
		{
			break;
		}
	}
	while( rc > 0 );

	//send the new command
	if(len > 0)
	{
		rc = libssh2_channel_write(channel, cmd, len);
//		qDebug() << "write: " << rc;
	}
	//get the response
	for( ;; )
	{
		/* loop until we block */ 
		int rc;
		do
		{
			rc = libssh2_channel_read(channel, &buffer[bytecount], sizeof(buffer)-bytecount);

			if( rc > 0 )
			{
				int i;
				bytecount += rc;
				iTimeout = 0;
			}
			else
			{
				if(rc == LIBSSH2_ERROR_EAGAIN)
				{
					//Check if we have received the response
					buffer[bytecount] = '\0';
					if((endsWith(buffer, nlPromptString1) != 0) ||
						(endsWith(buffer, nlPromptString2) != 0))
					{
						//The command was executed. Nothing more to wait for.
						rc = 0;
					}
					else
					{
//						QThread::msleep(100);
						if((iTimeout++ > 2) && (bytecount > 0))
						{
							//Send enter to get the commandprompt
							libssh2_channel_write(channel, "\n", 1);
						}
						if(iTimeout++ > 3)
						{
							rc = 0;
						}
					}
				}
			}
		}
		while( rc > 0 );
 
		/* this is due to blocking that would occur otherwise so we loop on
		   this condition */
		if( rc == LIBSSH2_ERROR_EAGAIN )
		{
			waitsocket(sock, session);
		}
		else
		{
			break;
		}
	}

	buffer[bytecount] = '\0';

	for(i = 0; (i < buflen-1) && (i < bytecount); i++)
	{
		buf[i] = buffer[i];
	}
	buf[i]='\0';
	return i;
}

static void sendCommand(char *cmdString)
{
	char* s;
	int cmdlen = strlen(cmdString);

	s = (char*)malloc(cmdlen + 2);
	if(s)
	{
		memset(s, 0, cmdlen + 2);

		strncpy(s, cmdString, cmdlen);
		append(s, "\n\0");

		execSSHCmd(s, strlen(s), s, 0);
		delete (s);
	}
}

void sendFullScreen(void)
{
	char *openCmd = "dbus-send --type=signal / com.gnome.mplayer.SetFullscreen boolean:'true'";
	sendCommand(openCmd);
}

void sendNormalScreen(void)
{
	char *openCmd = "dbus-send --type=signal / com.gnome.mplayer.SetFullscreen boolean:'false'";
	sendCommand(openCmd);
}

void sendPlay(void)
{
	char *openCmd = "dbus-send / com.gnome.mplayer.Play";
	sendCommand(openCmd);
}

void sendPause(void)
{
	char *openCmd = "dbus-send / com.gnome.mplayer.Pause";
	sendCommand(openCmd);
}

void sendStop(void)
{
	char *openCmd = "dbus-send / com.gnome.mplayer.Stop";
	sendCommand(openCmd);
}

void sendVolumeUp(void)
{
	char *openCmd = "dbus-send / com.gnome.mplayer.VolumeUp";
	sendCommand(openCmd);
}

void sendVolumeDown(void)
{
	char *openCmd = "dbus-send / com.gnome.mplayer.VolumeDown";
	sendCommand(openCmd);
}

void sendOpen(char *path)
{
	char *openCmd = "dbus-send --type=signal / com.gnome.mplayer.Open string:";
	char* s;
	int cmdlen = strlen(openCmd);
	int pathlen = strlen(path);

	s = (char*)malloc(cmdlen + pathlen + 5);
	if(s)
	{
		memset(s, 0, cmdlen + pathlen + 5);

		strncpy(s, openCmd, cmdlen);
		append(s, "'");
		append(s, path);
		append(s, "'\n\0");

		execSSHCmd(s, strlen(s), s, 0);
		delete (s);
	}
}

int connectHost(char *hostname, char *username, char *password)
{
	int rc;
	struct sockaddr_in sin;
	LIBSSH2_KNOWNHOSTS *nh;
	const char *fingerprint;
	size_t len;
	int type;
	unsigned short wVersionRequested;
	WSADATA wsaData;
	flagConnected = 0;
	channel = NULL;
	session= NULL;

	wVersionRequested = MAKEWORD(2, 2);

	if (WSAStartup(wVersionRequested, &wsaData) != 0) {
		/* Tell the user that we could not find a usable */
		/* Winsock DLL.								  */
		//"WSAStartup failed with error: " << WSAGetLastError();
		return -1;
	}
	rc = libssh2_init (0);
	if (rc != 0)
	{
		//"libssh2 initialization failed " << rc;
		return -1;
	}
 
	hostaddr = inet_addr(hostname);
 
	/* Ultra basic "connect to port 22 on localhost"
	 * Your code is responsible for creating the socket establishing the
	 * connection
	 */ 
	sock = socket(AF_INET, SOCK_STREAM, 0);
 
	sin.sin_family = AF_INET;
	sin.sin_port = htons(22);
	sin.sin_addr.s_addr = hostaddr;
	if (connect(sock, (struct sockaddr*)(&sin),
				sizeof(struct sockaddr_in)) != 0)
	{
		//"failed to connect! " << WSAGetLastError();
		return -1;
	}
 
	/* Create a session instance */ 
	session = libssh2_session_init();

	if (!session)
	{
		return -1;
	}
 
	/* tell libssh2 we want it all done non-blocking */ 
	libssh2_session_set_blocking(session, 0);

 
	/* ... start it up. This will trade welcome banners, exchange keys,
	 * and setup crypto, compression, and MAC layers
	 */ 
	while ((rc = libssh2_session_handshake(session, sock)) ==  LIBSSH2_ERROR_EAGAIN);
	if (rc)
	{
		//"Failure establishing SSH session: " << rc;
		return -1;
	}
 
	nh = libssh2_knownhost_init(session);

	if(!nh)
	{
		/* eeek, do cleanup here */ 
		return -1;
	}
 
	/* read all hosts from here */ 
	libssh2_knownhost_readfile(nh, "known_hosts", LIBSSH2_KNOWNHOST_FILE_OPENSSH);
 
	/* store all known hosts to here */ 
	libssh2_knownhost_writefile(nh, "dumpfile", LIBSSH2_KNOWNHOST_FILE_OPENSSH);
 
	fingerprint = libssh2_session_hostkey(session, &len, &type);

	if(fingerprint) 
	{
		struct libssh2_knownhost *host;
		int check = libssh2_knownhost_checkp(nh, hostname, 22,
											 fingerprint, len,
											 LIBSSH2_KNOWNHOST_TYPE_PLAIN | LIBSSH2_KNOWNHOST_KEYENC_RAW,
											 &host);
		/*****
		 * At this point, we could verify that 'check' tells us the key is
		 * fine or bail out.
		 *****/ 
	}
	else 
	{
		/* eeek, do cleanup here */ 
		return -1;
	}
	libssh2_knownhost_free(nh);

	if(strlen(password) != 0)
	{
		/* We could authenticate via password */ 
		while ((rc = libssh2_userauth_password(session, username, password)) == LIBSSH2_ERROR_EAGAIN);
		if (rc)
		{
			//"Authentication by password failed.\n";
			libssh2_session_disconnect(session, "Normal Shutdown, Thank you for playing");
			libssh2_session_free(session);
			closesocket(sock);
			libssh2_exit();
			WSACleanup();
			return -1;
		}
	}
	else
	{
		/* Or by public key */ 
		while ((rc = libssh2_userauth_publickey_fromfile(session, username,
														 "/home/user/"
														 ".ssh/id_rsa.pub",
														 "/home/user/"
														 ".ssh/id_rsa",
														 password)) == LIBSSH2_ERROR_EAGAIN);
		if (rc)
		{
			//"\tAuthentication by public key failed\n";
			libssh2_session_disconnect(session, "Normal Shutdown, Thank you for playing");
			libssh2_session_free(session);
			closesocket(sock);
			libssh2_exit();
			WSACleanup();
			return -1;
		}
	}
	/* Exec non-blocking on the remove host */ 
	while( (channel = libssh2_channel_open_session(session)) == NULL &&
		   libssh2_session_last_error(session,NULL,NULL,0) == LIBSSH2_ERROR_EAGAIN )
	{
		waitsocket(sock, session);
	}
	if( channel == NULL )
	{
		return -1;
	}
	
	while( (rc = libssh2_channel_request_pty(channel,"ansi")) == LIBSSH2_ERROR_EAGAIN )
	{
		waitsocket(sock, session);
	}
	if( rc != 0 )
	{
		return -1;
	}

	while( (rc = libssh2_channel_shell(channel)) == LIBSSH2_ERROR_EAGAIN )
	{
		waitsocket(sock, session);
	}
	if( rc != 0 )
	{
		return -1;
	}

	flagConnected = 1;

	waitForString("$");
	sshSetPrompt();

	return 0;
}

void disconnectHost()
{
	int rc;
	int exitcode;
	char *exitsignal=(char *)"none";
	if(channel != NULL)
	{
		while( (rc = libssh2_channel_close(channel)) == LIBSSH2_ERROR_EAGAIN )
		{
			waitsocket(sock, session);
		}
 
		if( rc == 0 )
		{
			exitcode = libssh2_channel_get_exit_status( channel );
			libssh2_channel_get_exit_signal(channel, &exitsignal, NULL, NULL, NULL, NULL, NULL);
		}
		libssh2_channel_free(channel);
		channel = NULL;
	}
 
	if(session != NULL)
	{
		libssh2_session_disconnect(session, "Normal Shutdown, Thank you for playing");
		libssh2_session_free(session);
		session= NULL;
	}
 
	closesocket(sock);
 
	libssh2_exit();
	WSACleanup();

	flagConnected = 0;
}