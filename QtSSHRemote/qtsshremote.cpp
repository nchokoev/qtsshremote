#include "qtsshremote.h"
#include <qdebug.h>
#include <QListWidgetItem>
#include <QThread>

QtSSHRemote::QtSSHRemote(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	flagConnected = false;

	ui.pbStartMPlayer->setEnabled(false);
	ui.pbKillMPlayer->setEnabled(false);

	connect(ui.pbConnect, SIGNAL(clicked()), this, SLOT(connectClicked()));
	connect(ui.pbOpen, SIGNAL(clicked()), this, SLOT(openClicked()));
	connect(ui.pbPlay, SIGNAL(clicked()), this, SLOT(playClicked()));
	connect(ui.pbStop, SIGNAL(clicked()), this, SLOT(stopClicked()));
	connect(ui.pbPause, SIGNAL(clicked()), this, SLOT(pauseClicked()));
	connect(ui.pbFullScreen, SIGNAL(clicked()), this, SLOT(fullScreenClicked()));
	connect(ui.pbNormalScreen, SIGNAL(clicked()), this, SLOT(normalScreenClicked()));
	connect(ui.pbVolumeUp, SIGNAL(clicked()), this, SLOT(volumeUpScreenClicked()));
	connect(ui.pbVolumeDown, SIGNAL(clicked()), this, SLOT(volumeDownScreenClicked()));
	connect(ui.pbStartMPlayer, SIGNAL(clicked()), this, SLOT(startmplayerClicked()));
	connect(ui.pbKillMPlayer, SIGNAL(clicked()), this, SLOT(killmplayerClicked()));
	connect(ui.pbDisconnect, SIGNAL(clicked()), this, SLOT(disconnectClicked()));
	connect(ui.lwFiles, SIGNAL(itemDoubleClicked(QListWidgetItem *)), 
		this, SLOT(itemDoubleClicked(QListWidgetItem *)));

	uiDisableConnected();
}

void QtSSHRemote::startmplayerClicked()
{
	char buf[1];
	QString s = "gnome-mplayer &\n";
	execSSHCmd((char*)s.toUtf8().constData(),s.size(), buf, 0);
	checkMplayerRunning();
}

void QtSSHRemote::killmplayerClicked()
{
	char buf[1];
	QStringList mp = getMPlayerRunning();
	if(mp.size() > 0)
	{
		foreach(QString s, mp)
		{
			QString kill = "kill ";
			kill.append(s);
			kill.append("\n");
 			execSSHCmd((char*)kill.toUtf8().constData(), kill.size(), buf, 0);
		}
	}
	checkMplayerRunning();
}

QtSSHRemote::~QtSSHRemote()
{

}

void QtSSHRemote::connectClicked()
{ 
	char buf[4096];
	ui.pbConnect->setEnabled(false);
	if(connectHost((char*)ui.leIPAddress->text().toUtf8().data(),
		(char*)ui.leUserName->text().toUtf8().data(),
		(char*)ui.lePassword->text().toUtf8().data()) == 0)
	{
	}
	else
	{
		ui.pbConnect->setEnabled(true);
		return;
	}

	QString s = "export DISPLAY=:0\n";
 	execSSHCmd((char*)s.toUtf8().constData(),s.size(), buf, 0);

	checkMplayerRunning();
	uiEnableConnected();
	refreshFolders();
}

void QtSSHRemote::checkMplayerRunning()
{
	QStringList mp = getMPlayerRunning();
	if(mp.isEmpty())
	{
		ui.pbStartMPlayer->setEnabled(true);
		ui.pbKillMPlayer->setEnabled(false);
	}
	else
	{
		char buf[1];
		ui.pbStartMPlayer->setEnabled(false);
		ui.pbKillMPlayer->setEnabled(true);
		QString s = "$(tr '\\0' '\\n' < /proc/$(pgrep gnome-mplayer | head -1 )/environ | sed -e 's/^/export /')\n";
 		execSSHCmd((char*)s.toUtf8().constData(),s.size(), buf, 0);
		s = "echo $DBUS_SESSION_BUS_ADDRESS\n";
 		execSSHCmd((char*)s.toUtf8().constData(),s.size(), buf, 0);
	}
}

QStringList QtSSHRemote::getMPlayerRunning()
{
	char buf[4096];
	QStringList mr;
	QStringList out;
	QString s = "pgrep gnome-mplayer\n";
	int sz = execSSHCmd((char*)s.toUtf8().constData(),s.size(), buf, 4096);
	mr =  QString(buf).remove(getPromptString()).remove('\r').split('\n');
	foreach (QString ins, mr)
	{
		if((ins != s.remove('\n')) && (!ins.isEmpty()))
		{
			out.append(ins);
		}
	}
	return out;
}

void QtSSHRemote::refreshFolders()
{
	char buf[4096];
	ui.lwFiles->clear();
	ui.lblCurPath->clear();
	int sz = execSSHCmd("pwd\n", 5, buf, 4096);
	QStringList sl = QString(buf).split('\n');
	if(sl.size() > 0)
	{
		ui.lblCurPath->setText(sl.at(1));
	}
	execSSHCmd("ls -1 --color=none\n", 20, buf, 4096);
	sl =  QString(buf).split('\n');
	new QListWidgetItem("..", ui.lwFiles);
	foreach(QString s, sl)
	{
		if((!s.contains("ls -1 --color=none")) && (!s.contains(getPromptString())))
		{
			new QListWidgetItem(s, ui.lwFiles);
		}
	}
}

void QtSSHRemote::openClicked()
{
	if(ui.lwFiles->selectedItems().size() == 1)
	{
		QString s(ui.lblCurPath->text().simplified());
		s.append("/");
		s.append(ui.lwFiles->selectedItems().at(0)->text().simplified());
		sendOpen((char*)s.toUtf8().constData());
	}
}

void QtSSHRemote::normalScreenClicked()
{
	sendNormalScreen();
}

void QtSSHRemote::volumeUpScreenClicked()
{
	sendVolumeUp();
}

void QtSSHRemote::volumeDownScreenClicked()
{
	sendVolumeDown();
}

void QtSSHRemote::fullScreenClicked()
{
	sendFullScreen();
}

void QtSSHRemote::playClicked()
{
	sendPlay();
}

void QtSSHRemote::stopClicked()
{
	sendStop();
}

void QtSSHRemote::pauseClicked()
{
	sendPause();
}

void QtSSHRemote::disconnectClicked()
{
	uiDisableConnected();
}

void QtSSHRemote::itemDoubleClicked(QListWidgetItem *item)
{
	char buf[1];
	QString s = item->text();
	s.replace(" ", "\\ ");
	s.replace("(", "\\(");
	s.replace(")", "\\)");
	s.replace("'", "\\'");
	s.prepend("cd ");
	s.append("\n");
	execSSHCmd((char*)s.toUtf8().constData(), s.size(), buf, 0);
	refreshFolders();
}

void QtSSHRemote::uiEnableConnected()
{
	ui.pbDisconnect->setEnabled(true);
	ui.pbConnect->setEnabled(false);

	ui.pbFullScreen->setEnabled(true);
	ui.pbOpen->setEnabled(true);
	ui.pbVolumeDown->setEnabled(true);
	ui.pbVolumeUp->setEnabled(true);
	ui.lwFiles->setEnabled(true);
	ui.pbPlay->setEnabled(true);
	ui.pbPause->setEnabled(true);
	ui.pbStop->setEnabled(true);
	ui.pbNormalScreen->setEnabled(true);
}

void QtSSHRemote::uiDisableConnected()
{
	ui.pbDisconnect->setEnabled(false);
	ui.pbConnect->setEnabled(true);

	ui.pbFullScreen->setEnabled(false);
	ui.pbOpen->setEnabled(false);
	ui.pbVolumeDown->setEnabled(false);
	ui.pbVolumeUp->setEnabled(false);
	ui.lwFiles->setEnabled(false);
	ui.pbPlay->setEnabled(false);
	ui.pbPause->setEnabled(false);
	ui.pbStop->setEnabled(false);
	ui.pbNormalScreen->setEnabled(false);
}
